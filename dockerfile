# Dockerfile for MinItinerary
# Project Source: https://gitlab.com/samuelfirst/minitinerary
# Deploying:
#  * Change the settings in settings.json to your liking
#  * Set the email and hostname in this file
#  * docker build .
#  * docker run -it -p 80:80 -p 443:443 <shasum>
FROM alpine

# HTTP(S)
EXPOSE 80
EXPOSE 443

# >>> SET THESE PRIOR TO DEPLOYING <<<
ENV EMAIL='my.email.here'
ENV DOMAIN='my.domain.name'
# >>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<

# Hijack python's path variable so we can avoid mucking about with the jenga
# tower build system
ENV PYTHONPATH=/opt/min

# Install Dependencies
RUN apk update && \
    apk add --no-cache certbot certbot-nginx python3 nginx && \
    ln -sf python3 /usr/bin/python && \
    python3 -m ensurepip --upgrade && \
    pip3 install --no-cache -U Flask Flask-HTTPAuth gunicorn

# Copy Files Into Container
ADD ./minitinerary.py /opt/min/minitinerary.py
ADD ./templates/ /opt/min/templates/
ADD ./entrypoint.sh /opt/min/entrypoint.sh

# Install nginx config
ADD ./nginx.conf /etc/nginx/nginx.my.conf

# Start minitinerary server
CMD ["/opt/min/entrypoint.sh"]
