#!/bin/sh

# Start NGINX proxy server with daemon off so it doesn't try to start a systemd
# service (which doesn't work in docker containers).  We also inject the domain
# name from $DOMAIN into the config file with sed. 
sed -e "$(printf 's/$domain/%s/g' $DOMAIN)" /etc/nginx/nginx.my.conf > /etc/nginx/nginx.sed.conf


# Install HTTPS certificate with certbot
# We need to temporarily start nginx with the default config
nginx -g "daemon off;" &
certbot -n -m "$EMAIL" --agree-tos --nginx certonly -d "$DOMAIN"
killall nginx

# Start actual nginx process
nginx -c "/etc/nginx/nginx.sed.conf" -g "daemon off;" &



# Start the gunicorn server with one worker because we use global state to hold
# the 'database' and don't want to create race conditions.  We want it to
# broadcast on localhost:8000 because we're sticking it behind NGINX.
gunicorn -w 1 -b "127.0.0.1:8000" "minitinerary:init()"
