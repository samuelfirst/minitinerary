from datetime import datetime, timedelta

class Schedule:
    def __init__(self, start, end):
        self.events = {}
        self.days = self._get_date_range(start, end)
        
        for day in self.days:
            key = day.date().isoformat()
            self.events[key] = []

    def _get_date_range(self, start, end):
        difference = (end - start).days + 1
        return [start + timedelta(n) for n in range(difference)]

    def add_event(self, event):
        day = event.start
        key = day.date().isoformat()
        pos = 0
        already_added = False
        
        for other_event in self.events[key]:
            if event > other_event:
                pos += 1
            else:
                if event == other_event:
                    already_added = True
                break
            
        if not already_added:
            self.events[key].insert(pos, event)

    def remove_event(self, day, event):
        self.events[day].remove(event)

    def get_events(self, day):
        key = day.date().isoformat()
        return self.events[key]

    def serialize(self):
        serial = {}

        for key in self.events.keys():
            serial[key] = []
            for event in self.events[key]:
                serial[key].append(event.serialize())

        return serial

    def deserialize(self, data):
        for key in data.keys():
            self.events[key] = [Event('', '', '', '').deserialize(datum) for
                                datum in data[key]]

class Event:
    def __init__(self, who, what, when_start, when_end):
        self.who   = who
        self.what  = what
        self.start = when_start
        self.end   = when_end

    def __eq__(self, other):
        return type(self) is type(other) and self.__dict__ == other.__dict__

    def __gt__(self, other):
        return type(self) is type(other) and self.start > other.start

    def serialize(self):
        return {"who":   self.who,
                "what":  self.what,
                "start": self.start.isoformat(),
                "end":   self.end.isoformat()}

    def deserialize(self, data):
        self.who   = data["who"]
        self.what  = data["what"]
        self.start = datetime.fromisoformat(data["start"])
        self.end   = datetime.fromisoformat(data["end"])
        return self
