#!/bin/env python3

import storage, schedule
from datetime import datetime
from flask import Flask, render_template, request
from markupsafe import escape, Markup
from flask_httpauth import HTTPBasicAuth

app = Flask(__name__)
auth = HTTPBasicAuth()
        
# Sets up the http basic auth verification
@auth.verify_password
def verify_password(username, password):
    if storage.users.validate(username, password):
        return username

# Handler for the Index Page
@app.route('/', methods=['GET', 'POST'])
@auth.login_required
def index():
    if request.method == 'POST':
        # Remove User
        if request.form["form-id"] == "admin-remove-user":
            if auth.current_user() == storage.settings["admin_username"]:
                storage.users.remove_user(escape(request.form["username"]))

        # Add User
        if request.form["form-id"] == "admin-add-user":
            if auth.current_user() == storage.settings["admin_username"]:
                storage.users.add_user(escape(request.form["username"]),
                                       escape(request.form["password"]))

        # Remove Event
        if request.form["form-id"] == "remove-event":
            who   = escape(request.form["remove-event-who"])
            what  = escape(request.form["remove-event-what"])
            start = escape(request.form["remove-event-start"])
            end   = escape(request.form["remove-event-end"])
            day   = escape(request.form["remove-event-day"])
            start_iso = datetime.fromisoformat(f"{day}T{start}")
            end_iso   = datetime.fromisoformat(f"{day}T{end}")
            if auth.current_user() in [storage.settings["admin_username"], who]:
                storage.schedule.remove_event(day, schedule.Event(who, what,
                                                                  start_iso,
                                                                  end_iso))

        # Add Event
        if request.form["form-id"] == "submit-event":
            who   = auth.current_user()
            what  = escape(request.form["submit-event-what"])
            start = escape(request.form["submit-event-start"])
            end   = escape(request.form["submit-event-end"])
            day   = escape(request.form["submit-event-day"])
            start_iso = datetime.fromisoformat(f"{day}T{start}")
            end_iso   = datetime.fromisoformat(f"{day}T{end}")
            storage.schedule.add_event(schedule.Event(who, what,
                                                      start_iso,
                                                      end_iso))

        # Sync the database to disk after processing changes
        storage.sync_to_disk()
        storage.sync_conf_to_disk()

    # Render and return the page
    return render_template('index.html',
                           settings=storage.settings,
                           user=auth.current_user(),
                           users=storage.users,
                           schedule=storage.schedule)

# Spin up the app
def init():
    storage.init()
    return app

if __name__ == '__main__':
    storage.init()
    app.run(debug=True)
