import json
import os
from datetime import datetime
import schedule as sched
import user

schedule = None
settings = {}
users    = user.UserTable()
conf_path = "./settings.json"

def init():
    global schedule
    sync_conf_from_disk()
    schedule = sched.Schedule(datetime.fromisoformat(settings["start_date"]),
                              datetime.fromisoformat(settings["end_date"]))
    
    if db_exists():
        sync_from_disk()
    else:
        create_db()
        sync_from_disk()

def db_exists():
    return os.path.exists(settings["db_path"])

def create_db():
    for user, password in settings["users"]:
        users.add_user(user, password)
    sync_to_disk()

# Write the config file to settings.json
def sync_conf_to_disk():
    with open(conf_path, 'w') as conf:
        conf.write(json.dumps(settings))

# Load the config file from settings.json
def sync_conf_from_disk():
    global settings
    with open(conf_path, 'r') as conf:
        settings = json.loads(conf.read())

        
# Write the database to the disk
def sync_to_disk():
    data = {"schedule": schedule.serialize(),
            "users":    users.serialize()}
    
    with open(settings["db_path"], 'w') as db:
        db.write(json.dumps(data))

# Load the database from the disk
def sync_from_disk():
    global schedule, users
    data = {}
    
    with open(settings["db_path"], 'r') as db:
        data = json.loads(db.read())

    schedule.deserialize(data["schedule"])
    users.deserialize(data["users"])
