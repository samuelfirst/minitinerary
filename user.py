class UserTable:
    def __init__(self):
        self.table = []

    def __iter__(self):
        for entry in self.table:
            yield entry

    def validate(self, user, password):
        if user in self.table:
            return self.table[self.table.index(user)].validate(user, password)

    def add_user(self, user, password):
        self.table.append(User(user, password))

    def remove_user(self, user):
        print(self.table)
        if user in self.table:
            print(user)
            self.table.remove(user)

    def serialize(self):
        return [entry.serialize() for entry in self.table]

    def deserialize(self, data):
        self.table = [User('','').deserialize(entry) for entry in data]

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    # Equality check to see if username is equal to a passed string
    # >>>> NOT FOR VALIDATION <<<<
    def __eq__(self, other):
        return self.username == other

    def validate(self, username, password):
        return self.username == username and self.password == password

    def serialize(self):
        return {"username": self.username, "password": self.password}

    def deserialize(self, data):
        self.username = data["username"]
        self.password = data["password"]
        return self
