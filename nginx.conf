# NGINX Proxy Config
# Based on the configs from the gunicorn docs found here:
#  https://docs.gunicorn.org/en/latest/deploy.html
worker_processes 1;

# Setting user as nobody nogroup so the nginx process doesn't have shell
user nobody nogroup;

# Set locations for error log and pid file
error_log /var/log/nginx/error.log warn;
pid /var/run/nginx.pid;

# Set number of worker connections
events {
  worker_connections 1024;
  accept_mutex off;
}

http {
  # Set up MIME types
  # For more information, see:
  #  https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_Types
  include mime.types;
  default_type application/octet-stream;

  # Set location for access log
  access_log /var/log/nginx/access.log combined;

  # Use the sendfile() system call for efficiency
  sendfile on;

  # Add the gunicorn server
  upstream gunicorn_server {
    server 127.0.0.1:8000 fail_timeout=0;
  }

  # Prevent Host Header Spoofing
  server {
    server_name _;
    listen 80 default_server;
    listen 443 ssl default_server;
    ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    return 444;
  }

  server {
    # Listen on 80 for HTTP and 443 for HTTPS
    listen 80;
    listen 443 ssl http2;

    # Add SSL certificate and key
    # SET THE DOMAIN NAME BELOW
    ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;

    # Set the maximum size of the client request body
    client_max_body_size 10M;

    # SET THE HOSTNAME BELOW
    server_name $domain;

    keepalive_timeout 5;

    # Forward everything to gunicorn
    location / {
      try_files $uri @proxy_to_gunicorn;
    }

    # Forward Headers to Gunicorn
    location @proxy_to_gunicorn {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header Host $http_host;
      proxy_redirect off;
      proxy_pass http://gunicorn_server;
    }
  }
}

