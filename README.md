# minitinerary

MinItinerary is a tiny webforms based itinerary website.  It is designed to be
quickly and easily deployed for short periods of time.

The problem this project is designed to solve is as follows: When I return home, 
there are too many people who want to spend time with me.  To spend time with 
them, I have to continuously adjust my schedule, which I find stressful and
tedious.

MinItinerary solves this problem by making my itinerary available to the people
that I want to visit, enabling them to schedule time with me when it is
convenient for everyone involved.

## Deployment

### Prerequisites

Prior to deploying, you will need the following:
- A server that has:
  - A public ip address
  - Open ports on 80 and 443
- A domain name with an A record pointing to the server's ip address
  - Subdomains are also acceptable

### Deploying

- [ ] Clone the git repository
```
git clone https://gitlab.com/samuelfirst/minitinerary
cd minitinerary
```
- [ ] Edit the environment variables in the marked section of the dockerfile
```
<text editor> ./dockerfile
```
- [ ] Build the docker image
```
docker build .
```
- [ ] Run the docker image
```
docker run -it -p 80:80 -p 443:443 <hash>
```

### Hand Modifying the Itinerary

- [ ] Get a shell in the container
```
docker exec -it <hash> /bin/sh
```

- [ ] Edit the `db.json` file
```
vi /db.json
```

- [ ] Find gunicorn pid
```
pidof gunicorn
```

- [ ] Reload gunicorn
```
kill -HUP <pid>
```

## Notes
- When using an Oracle VPS as the server, follow section three of [this](https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/apache-on-ubuntu/01oci-ubuntu-apache-summary.htm) guide to open ports 80 and 443.

## License
All the code in this repository is licensed under the terms of the GPLv3.  See
`./LICENSE` for the terms of the license.
